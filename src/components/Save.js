import React, { useEffect, useState } from 'react';
import { useDataFiles } from '../hooks/datafiles';

const TITLE = document.title;

function listener(e) {
	e.preventDefault();
}

function download(json, name) {
	const url = URL.createObjectURL(new Blob([JSON.stringify(json)], { type: 'application/json' }));
	const a = document.createElement('a');
	a.href = url;
	a.download = name;
	const onClick = () => {
		setTimeout(() => {
			URL.revokeObjectURL(url);
			a.removeEventListener('click', onClick);
		}, 1);
	};
	a.addEventListener('click', onClick);
	a.click();
}

export default function Save() {
	const { active, initialActive } = useDataFiles();
	const [lastSaved, setLastSaved] = useState(initialActive);
	const modified = active !== lastSaved;
	const name = active?.name;
	useEffect(() => {
		let title = TITLE;
		if(name) {
			if(modified) {
				title += '*';
			}
			title += ' ';
			title += name;
		}
		document.title = title;
		if(modified) {
			window.addEventListener('beforeunload', listener);
			return () => window.removeEventListener('beforeunload', listener);
		}
	}, [modified, name]);
	return <button disabled={!modified} onClick={e => {
		e.preventDefault();
		download(active.json, name);
		setLastSaved(active);
	}}>Save</button>;
}
