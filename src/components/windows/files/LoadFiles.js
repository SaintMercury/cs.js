import React, { useState } from 'react';
import randomColor from 'randomcolor';

function byLastModified(lhs, rhs) {
	return lhs.lastModified - rhs.lastModified;
}

function parseFiles(files) {
	return Promise.all(files.map(async file => {
		if(file instanceof File) {
			const text = await file.text();
			return {
				name: file.name,
				json: JSON.parse(text),
				color: randomColor({ luminosity: 'light', seed: file.name })
			};
		}
		return file;
	}));
}

export default function LoadFiles({ onClose, onSubmit, files: initialFiles }) {
	const [files, setFiles] = useState(initialFiles);
	const [error, setError] = useState(null);
	const [disabled, setDisabled] = useState(false);
	return <form onSubmit={e => {
		e.preventDefault();
		setDisabled(true);
		setError(null);
		parseFiles(files).then(onSubmit).catch(err => {
			setError(err);
			setDisabled(false);
		});
	}}>
		{files.length > 0 ? <table>
			<tbody>
				{files.map((file, index) => {
					const move = amount => e => {
						e.preventDefault();
						setFiles(prev => {
							const clone = prev.slice();
							const f = clone[index];
							clone[index] = clone[index + amount];
							clone[index + amount] = f;
							return clone;
						});
					};
					return <tr key={file.name}>
						<td className="text-right">{index + 1}.</td>
						<td>{file.name}</td>
						<td>
							<button disabled={disabled || index === 0} onClick={move(-1)}>&uarr;</button>
						</td>
						<td>
							<button disabled={disabled || index === files.length - 1} onClick={move(1)}>&darr;</button>
						</td>
						<td>
							<button disabled={disabled} onClick={e => {
								e.preventDefault();
								setFiles(prev => prev.filter(f => f.name !== file.name));
							}}>Delete</button>
						</td>
					</tr>;
				})}
			</tbody>
		</table> : <p>No files selected.</p>}
		<label>
			<input type="file" accept=".json,application/json" multiple hidden disabled={disabled} onChange={e => {
				if(e.target.files.length) {
					setFiles(prev => {
						return prev.concat([...e.target.files].filter(file => !prev.some(f => f.name === file.name)).sort(byLastModified));
					});
				}
			}} />
			<span className="btn" disabled={disabled}>Add File</span>
		</label>
		<div className="load-files-controls">
			{error && <p>{error.message}</p>}
			<button onClick={e => {
				e.preventDefault();
				onClose();
			}} disabled={disabled}>Cancel</button>
			{' '}
			<input type="submit" value="Load" disabled={disabled} />
			{disabled && <> <progress /></>}
		</div>
	</form>;
}
