import React, { memo } from 'react';
import { FILTER_COMPARISONS, FILTER_FUNCTIONS, FILTER_TYPES } from '../../../../constants';
import { useActors, useCells, useClasses, useFactions, useGlobals, useItems, useJournals, useLocals, useRaces } from '../../../../hooks/dialogue';

const MULTIPLE = {
	toString() {
		return '';
	}
};

function getFilterValue(record, slot, key) {
	const filter = record.record.filters?.find(f => f.slot === `Slot${slot}`);
	return filter?.[key] ?? '';
}

function getValue(value) {
	if(typeof value !== 'object') {
		return '';
	} else if('Integer' in value) {
		return value.Integer;
	}
	return value.Float ?? '';
}

function toValue(value, valueType) {
	const type = valueType === 'Float' ? 'Float' : 'Integer';
	if(isNaN(value) || value === '') {
		return { [type]: value };
	}
	const f = +value;
	if(Number.isInteger(f)) {
		return { [type]: f };
	}
	return { Float: f };
}

function identity(a) {
	return a;
}

const MemoIdSelect = memo(({ values, value, onChange, readOnly }) => {
	if(value && value !== MULTIPLE && !values.includes(value)) {
		const lower = value.toLowerCase();
		value = values.find(v => v.toLowerCase() === lower);
	}
	return <select onChange={onChange} value={value} disabled={readOnly}>
		{value === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
		<option value=""></option>
		{values.map(v => <option key={v} value={v}>{v}</option>)}
	</select>;
});

function IdSelect({ use, ...props }) {
	const values = use();
	return <MemoIdSelect {...props} values={values} />;
}

function getFactions() {
	const factions = useFactions();
	return factions.map(f => f.id);
}

function FunctionOrId({ type, set, slot, functionOrId, variable, globals, locals, readOnly }) {
	if(type === 'Function') {
		return <select value={functionOrId} onChange={e => set(slot, 'filter_function', e.target.value)} disabled={readOnly}>
			{functionOrId === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
			<option value=""></option>
			{Object.entries(FILTER_FUNCTIONS).map(([value, label]) => {
				return <option key={value} value={value}>{label}</option>;
			})}
		</select>;
	}
	const onChange = e => set(slot, 'id', e.target.value);
	if(type === 'Global') {
		return <IdSelect use={() => globals.map(g => g.id)} value={variable?.id || functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'Local' || type === 'NotLocal') {
		return <IdSelect use={() => locals.map(l => l.id)} value={variable?.id || functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'Journal') {
		return <IdSelect use={useJournals} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'Item') {
		return <IdSelect use={useItems} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'Dead' || type === 'NotId') {
		return <IdSelect use={useActors} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'NotFaction') {
		return <IdSelect use={getFactions} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'NotClass') {
		return <IdSelect use={useClasses} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'NotRace') {
		return <IdSelect use={useRaces} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	} else if(type === 'NotCell') {
		return <IdSelect use={useCells} value={functionOrId} onChange={onChange} readOnly={readOnly} />;
	}
	return <input type="text" value={functionOrId} disabled={!type || type === MULTIPLE} onChange={onChange} readOnly={readOnly} />;
}

function getVariable(globals, locals, type, functionOrId) {
	if(functionOrId && functionOrId !== MULTIPLE) {
		let variables;
		if(type === 'Global') {
			variables = globals;
		} else if(type === 'Local' || type === 'NotLocal') {
			variables = locals;
		}
		if(variables) {
			const lower = functionOrId.toLowerCase();
			return variables.find(v => v.id.toLowerCase() === lower);
		}
	}
}

export default function FilterEditor({ slot, records, set, readOnly = false }) {
	const globals = useGlobals();
	const locals = useLocals();
	const get = (name, mapper = identity) => {
		const value = mapper(getFilterValue(records[0], slot, name));
		if(records.some(record => mapper(getFilterValue(record, slot, name)) !== value)) {
			return MULTIPLE;
		}
		return value;
	};
	const type = get('filter_type');
	const functionOrId = get(type === 'Function' ? 'filter_function' : 'id');
	const comparison = get('filter_comparison');
	const filterValue = get('value', getValue);
	const variable = getVariable(globals, locals, type, functionOrId);
	const valueType = variable?.type;
	const changeValue = e => set(slot, 'value', toValue(e.target.value, valueType));
	return <>
		<td>
			<select value={type} onChange={e => set(slot, 'filter_type', e.target.value)} disabled={readOnly}>
				{type === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
				<option value=""></option>
				{Object.entries(FILTER_TYPES).map(([value, label]) => {
					return <option key={value} value={value}>{label}</option>;
				})}
			</select>
		</td>
		<td>
			<FunctionOrId type={type} set={set} slot={slot} functionOrId={functionOrId} variable={variable} globals={globals} locals={locals} readOnly={readOnly} />
		</td>
		<td>
			<select value={comparison} disabled={!functionOrId || readOnly} onChange={e => set(slot, 'filter_comparison', e.target.value)}>
				{comparison === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
				<option value=""></option>
				{Object.entries(FILTER_COMPARISONS).map(([value, label]) => {
					return <option key={value} value={value}>{label}</option>;
				})}
			</select>
		</td>
		<td><input type="number" value={filterValue} disabled={!comparison} onChange={changeValue} step={valueType && valueType !== 'Float' ? 1 : undefined} readOnly={readOnly} /></td>
	</>;
}
