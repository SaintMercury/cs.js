import React, { memo } from 'react';

export function getValue(record, key, data, def = '') {
	if(data) {
		return record.record.data?.[key] ?? def;
	}
	return record.record[key] ?? def;
}

export const MULTIPLE = {
	toString() {
		return '[Multiple]';
	}
};

export function getValues(records, name, data, def) {
	const value = getValue(records[0], name, data, def);
	if(records.some(record => value !== getValue(record, name, data, def))) {
		return MULTIPLE;
	}
	return value;
}

const MemoRecordInput = memo(({ value, set, name, type = 'text', data, use, readOnly, values, ...props }) => {
	const onChange = e => set(name, e.target.value, data);
	if(type === 'textarea') {
		return <textarea value={value === MULTIPLE ? value.toString() : value} onChange={onChange} {...props} readOnly={readOnly} />;
	} else if(type === 'select') {
		return <select value={value === MULTIPLE ? value.toString() : value} onChange={onChange} {...props} disabled={props.disabled || readOnly} />;
	} else if(use) {
		if(value && value !== MULTIPLE && !values.includes(value)) {
			const lower = value.toLowerCase();
			value = values.find(v => v.toLowerCase() === lower);
		}
		return <select {...props} onChange={onChange} value={value} disabled={props.disabled || readOnly}>
			{value === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
			<option value=""></option>
			{values.map(v => <option key={v} value={v}>{v}</option>)}
		</select>;
	}
	return <input value={value === MULTIPLE ? value.toString() : value} onChange={onChange} type={type} readOnly={readOnly} {...props} />;
});

export default function RecordInput({ records, data = false, def = '', ...props }) {
	const { use, name } = props;
	const values = use && use();
	const value = getValues(records, name, data, def);
	return <MemoRecordInput {...props} values={values} value={value} data={data} />;
}
