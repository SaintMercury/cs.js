import React, { memo, useMemo } from 'react';
import TopicEditor from './TopicEditor';
import JournalEditor from './JournalEditor';
import { FILTER_TYPE_FUNCTIONS } from '../../../../constants';

function setOrDelete(object, key, value) {
	if(value === undefined) {
		delete object[key];
	} else {
		object[key] = value;
		if(key === 'quest_name') {
			delete object.quest_finish;
			delete object.quest_restart;
		} else if(key === 'quest_finish') {
			delete object.quest_name;
			delete object.quest_restart;
		} else if(key === 'quest_restart') {
			delete object.quest_name;
			delete object.quest_finish;
		}
	}
}

function getSetters(setRecords) {
	return {
		set(key, value, data) {
			setRecords(line => {
				const prevValue = data ? line.record?.data?.[key] : line.record?.[key];
				if(typeof value === 'function') {
					value = value(prevValue);
				}
				if(value === prevValue) {
					return line;
				}
				const record = { ...line.record };
				if(data) {
					record.data = { ...record.data };
					setOrDelete(record.data, key, value);
				} else {
					setOrDelete(record, key, value);
				}
				return { ...line, record };
			});
		},
		setFilter(slot, key, value) {
			setRecords(line => {
				const record = { ...line.record };
				let index;
				if(!record.filters) {
					record.filters = [];
					index = -1;
				} else {
					record.filters = record.filters.slice();
					index = record.filters.findIndex(f => f.slot === `Slot${slot}`);
				}
				let filter;
				if(index >= 0) {
					filter = { ...record.filters[index] };
					record.filters[index] = filter;
				} else {
					filter = { slot: `Slot${slot}` };
					record.filters.push(filter);
				}
				if(key === 'filter_type' && filter[key] !== key) {
					filter.id = '';
					filter.filter_function = value === 'Function' ? '' : FILTER_TYPE_FUNCTIONS[value];
					if(!value) {
						delete filter.filter_comparison;
						delete filter.value;
					}
				}
				filter[key] = value;
				return { ...line, record };
			});
		}
	};
}

function InfoEditor({ records, setRecords, type, deleteRecords, addLine, moveSelection, firstSelected, lastSelected, overrideSelection }) {
	const readOnly = records.some(r => !r.file.active);
	const { set, setFilter } = useMemo(() => getSetters(setRecords), [setRecords]);
	if(type === 'Journal') {
		return <JournalEditor
			records={records}
			set={set}
			type={type}
			readOnly={readOnly}
			deleteRecords={deleteRecords}
			overrideSelection={overrideSelection}
			addLine={addLine}
			moveSelection={moveSelection}
			firstSelected={firstSelected}
			lastSelected={lastSelected} />;
	}
	return <TopicEditor
		records={records}
		set={set}
		type={type}
		setFilter={setFilter}
		readOnly={readOnly}
		deleteRecords={deleteRecords}
		overrideSelection={overrideSelection}
		addLine={addLine}
		moveSelection={moveSelection}
		firstSelected={firstSelected}
		lastSelected={lastSelected} />;
}

export default memo(InfoEditor);
