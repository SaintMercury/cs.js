import React from 'react';
import { FILTER_COMPARISONS, FILTER_FUNCTIONS, FILTER_TYPES } from '../../../constants';

function FilterValue({ value }) {
	if(typeof value === 'object') {
		if('Integer' in value) {
			return value.Integer;
		} else if('Float' in value) {
			return value.Float;
		}
	}
	return null;
}

export default function Filter({ filter }) {
	if(!filter) {
		return <td />;
	}
	return <td className="filter">
		{filter.filter_type === 'Function' ? FILTER_FUNCTIONS[filter.filter_function] : FILTER_TYPES[filter.filter_type]}{' '}
		{filter.id} {FILTER_COMPARISONS[filter.filter_comparison]} <FilterValue value={filter.value} />
	</td>;
}
