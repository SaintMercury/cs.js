import React, { memo } from 'react';
import InfoRow, { Header, getColumns } from './InfoRow';

function InfoList({ records, setRecords, type, addLine }) {
	return <table>
		<Header type={type} />
		<tbody className={type === 'Journal' ? 'journal' : 'info'}>
			{records.map((record, index) => <InfoRow key={record.record.info_id} {...record} index={index} setRecords={setRecords} type={type} />)}
		</tbody>
		{addLine && <tfoot>
			<tr>
				<td colSpan={getColumns(type)} className="text-center">
					<button onClick={e => {
						e.preventDefault();
						addLine(records.length);
					}}>Add entry</button>
				</td>
			</tr>
		</tfoot>}
	</table>;
}

export default memo(InfoList);
