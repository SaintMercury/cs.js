import React, { memo, useEffect, useMemo } from 'react';
import InfoEditor from './editor/InfoEditor';
import InfoList from './InfoList';
import { useDataFiles } from '../../../hooks/datafiles';

const EMPTY_RECORDS = [{
	record: {
		type: 'Info',
		flags: [0, 0],
		info_id: '',
		prev_id: '',
		next_id: '',
		text: '',
		filters: []
	}
}];
const BUFFER = new window.BigUint64Array(1);

function generateId() {
	crypto.getRandomValues(BUFFER);
	return BUFFER[0].toString();
}

function getAddLine(setRecords, initialActive) {
	return (index, defaults = EMPTY_RECORDS) => {
		setRecords(prev => {
			let wasSelected = false;
			const out = prev.map((r, i) => {
				if(r.selected) {
					wasSelected = true;
					if(index === true) {
						index = i;
					}
					return { ...r, selected: false };
				} else if(index === false && wasSelected) {
					index = i;
				}
				return r;
			});
			if(index === false) {
				index = prev.length;
			}
			out.splice(index, 0, ...defaults.map(({ record }) => {
				return {
					selected: true,
					record: { ...record, info_id: generateId() },
					file: initialActive
				};
			}));
			return out;
		});
	};
}

function getMove(setRecords) {
	return amount => setRecords(prev => {
		let start = -1;
		let end = -1;
		for(let i = 0; i < prev.length; i++) {
			const record = prev[i];
			if(record.selected) {
				if(start < 0) {
					start = i;
				}
			} else if(start >= 0) {
				end = i;
				break;
			}
		}
		if(start >= 0) {
			if(end === -1) {
				end = prev.length;
			}
		} else {
			return prev;
		}
		const copy = prev.slice();
		const selection = copy.splice(start, end - start);
		let index;
		if(typeof amount === 'string') {
			index = copy.findIndex(r => r.record.info_id === amount) + 1;
		} else if(amount < 0) {
			index = Math.max(start + amount, 0);
		} else {
			index = Math.min(start + amount, copy.length);
		}
		copy.splice(index, 0, ...selection);
		return copy;
	});
}

function getOverride(setRecords, files) {
	return () => {
		const file = files.find(f => f.active);
		setRecords(prev => prev.map(r => {
			if(r.selected && !r.file.active) {
				return { ...r, file, override: true };
			}
			return r;
		}));
	};
}

function getKeyPressHandler(setRecords) {
	return e => {
		if(e.target === document.body && (e.key === 'ArrowDown' || e.key === 'ArrowUp')) {
			e.preventDefault();
			const direction = e.key === 'ArrowDown';
			const extend = e.shiftKey;
			setRecords(prev => {
				if(!prev?.length) {
					return prev;
				}
				let firstIndex = Infinity;
				let lastIndex = -Infinity;
				for(let i = 0; i < prev.length; i++) {
					if(prev[i].selected) {
						firstIndex = Math.min(i, firstIndex);
						lastIndex = Math.max(i, lastIndex);
					} else if(Number.isFinite(lastIndex)) {
						break;
					}
				}
				if(Number.isFinite(firstIndex)) {
					const index = direction ? lastIndex + 1 : firstIndex - 1;
					return prev.map((r, i) => {
						if(i === index) {
							return { ...r, selected: true };
						} else if(!extend && r.selected) {
							return { ...r, selected: false };
						}
						return r;
					});
				}
				const index = direction ? 0 : prev.length - 1;
				const copy = prev.slice();
				copy[index] = { ...copy[index], selected: true };
				return copy;
			});
		}
	};
}

function TopicWindow({ records, setRecords, type, inTopic, deleteRecords, visible }) {
	const { initialActive, files } = useDataFiles();
	const { setSelectedRecords, addLine, moveSelection, overrideSelection, onKeyPress } = useMemo(() => {
		return {
			setSelectedRecords: mapper => setRecords(prev => prev.map(r => {
				if(r.selected) {
					return mapper(r);
				}
				return r;
			})),
			addLine: getAddLine(setRecords, initialActive),
			moveSelection: getMove(setRecords),
			overrideSelection: getOverride(setRecords, files),
			onKeyPress: getKeyPressHandler(setRecords)
		};
	}, [setRecords, initialActive, files]);
	useEffect(() => {
		if(visible) {
			window.addEventListener('keydown', onKeyPress);
			return () => window.removeEventListener('keydown', onKeyPress);
		}
	}, [visible, onKeyPress]);
	const selectedRecords = records.filter(r => r.selected);
	return <div className="topics">
		<div className="info">
			<InfoList records={records} setRecords={setRecords} type={type} addLine={inTopic && addLine} />
		</div>
		{selectedRecords.length > 0 && <InfoEditor
			records={selectedRecords}
			setRecords={setSelectedRecords}
			type={type}
			addLine={addLine}
			firstSelected={records[0] === selectedRecords[0]}
			lastSelected={records[records.length - 1] === selectedRecords[selectedRecords.length - 1]}
			moveSelection={moveSelection}
			overrideSelection={overrideSelection}
			deleteRecords={deleteRecords} />}
	</div>;
}

export default memo(TopicWindow);
