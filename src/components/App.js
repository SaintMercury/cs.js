import React from 'react';
import Navigation from './Navigation';
import Content from './Content';

export default function App() {
	return <>
		<Navigation />
		<div id="content">
			<Content />
		</div>
	</>;
}
