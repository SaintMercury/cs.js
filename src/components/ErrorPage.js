import React, { Component, useEffect } from 'react';
import { backup } from '../hooks/datafiles';

function ErrorMessage({ error }) {
	const { active } = backup;
	useEffect(() => {
		console.error(error);
	}, [error]);
	let data = '';
	if(active?.json) {
		try {
			data = JSON.stringify(active.json);
		} catch(err) {
			console.warn(err);
		}
	}
	return <div>
		<h1>Grats, you broke it</h1>
		<p>Kindly report whatever error is in your browser's console.</p>
		{data ? <>
			<p>This might be a working file:</p>
			<textarea value={data} readOnly />
		</> : <p>Failed to recover any data.</p>}
	</div>;
}

export default class ErrorPage extends Component {
	constructor(props) {
		super(props);
		this.state = { error: null };
	}

	static getDerivedStateFromError(error) {
		return { error };
	}

	render() {
		if(this.state.error) {
			return <ErrorMessage error={this.state.error} />;
		}
		return this.props.children;
	}
}
