import { useEffect, useState } from 'react';

const EMPTY_CACHE = {
	files: [],
	cached: []
};

export function useCached(files, transform) {
	const [state, setState] = useState(EMPTY_CACHE);
	useEffect(() => {
		setState(prev => {
			let out = prev;
			for(let i = 0; i < files.length; i++) {
				if(files[i] !== prev.files[i]) {
					const value = transform(files[i].json);
					if(out === prev) {
						out = {
							files,
							cached: prev.cached.slice(0, files.length)
						};
					}
					out.cached[i] = value;
				}
			}
			if(out.files.length > files.length) {
				out = {
					files,
					cached: prev.cached.slice(0, files.length)
				};
			}
			return out;
		});
	}, [files, transform]);
	return state.cached;
}
