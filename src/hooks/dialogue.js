import React, { useContext, useEffect, useMemo, useState } from 'react';
import { useCached } from './cache';
import { useDataFiles } from './datafiles';
import { FILTER_COMPARISONS, FILTER_TYPES, FILTER_FUNCTIONS, FLAG_CAN_CARRY, FLAG_DELETED, FLAG_IGNORED, FILTER_TYPE_FUNCTIONS } from '../constants';

const Context = React.createContext();
const EMPTY_STATE = {
	actors: [],
	cells: [],
	classes: [],
	factions: [],
	globals: [],
	items: [],
	journals: [],
	locals: [],
	races: [],
	topics: {},
	types: {}
};
const RECORDS = {
	Alchemy: 'items',
	Apparatus: 'items',
	Armor: 'items',
	Book: 'items',
	Cell: 'cells',
	Class: 'classes',
	Clothing: 'items',
	Creature: 'actors',
	Ingredient: 'items',
	Lockpick: 'items',
	MiscItem: 'items',
	Npc: 'actors',
	Probe: 'items',
	Race: 'races',
	Region: 'cells',
	RepairTool: 'items',
	Weapon: 'items'
};
const SCRIPT_VARIABLE = /^[\s,]*(short|long|float|"short"|"long"|"short")[\s,]+([^;\s]+)?/i;
const EMPTY_TOPIC = {
	type: 'Dialogue',
	flags: [0, 0]
};

function byId([lhs], [rhs]) {
	if(lhs > rhs) {
		return 1;
	} else if(lhs < rhs) {
		return -1;
	}
	return 0;
}

function addId(map, id) {
	if(id) {
		const canonical = id.toLowerCase();
		map[canonical] = id;
	}
}

function addVariable(map, id, type) {
	if(id && type) {
		const canonical = id.toLowerCase();
		if(type !== 'Float' && map[canonical]?.type === 'Float') {
			type = 'Float';
		}
		map[canonical] = { id, type };
	}
}

function addFaction(map, id, ranks) {
	if(id) {
		const canonical = id.toLowerCase();
		if(!map[canonical]) {
			map[canonical] = { ranks: [] };
		}
		map[canonical].id = id;
		if(ranks?.length) {
			map[canonical].ranks = ranks;
		}
	}
}

function removeQuotes(value) {
	if(value.startsWith('"') && value.endsWith('"')) {
		return value.slice(1, -1);
	}
	return value;
}

function parseScript(script) {
	const out = [];
	script.text.split('\n').forEach(line => {
		const groups = line.match(SCRIPT_VARIABLE);
		if(groups) {
			groups.shift();
			let [type, name] = groups;
			if(name && type) {
				name = removeQuotes(name);
				type = removeQuotes(type).toLowerCase();
				if(name && type) {
					if(type === 'short') {
						type = 'Short';
					} else if(type === 'long') {
						type = 'Long';
					} else {
						type = 'Float';
					}
					out.push({ name, type });
				}
			}
		}
	});
	return out;
}

function fillCache(files) {
	const maps = {};
	for(const key in EMPTY_STATE) {
		maps[key] = {};
	}
	files.forEach(file => {
		file.json.forEach(record => {
			if(record.type === 'Dialogue') {
				if(record.dialogue_type === 'Journal') {
					addId(maps.journals, record.id);
				}
				if(!maps.types[record.dialogue_type]) {
					maps.types[record.dialogue_type] = {};
				}
				addId(maps.types[record.dialogue_type], record.id);
			} else if(record.type === 'Faction') {
				addFaction(maps.factions, record.id, record.rank_names);
			} else if(record.type === 'GlobalVariable') {
				addVariable(maps.globals, record.id, record.value_type || record.global_type);
			} else if(record.type === 'Info') {
				addId(maps.actors, record.speaker_id);
				addId(maps.cells, record.speaker_cell);
				addId(maps.classes, record.speaker_class);
				addFaction(maps.factions, record.speaker_faction);
				addFaction(maps.factions, record.player_faction);
				addId(maps.races, record.speaker_rank);
				record.filters?.forEach(filter => {
					if(filter.filter_type === 'Global') {
						addVariable(maps.globals, filter.id, Object.keys(filter.value ?? {})[0]);
					} else if(filter.filter_type === 'Local' || filter.filter_type === 'NotLocal') {
						addVariable(maps.locals, filter.id, Object.keys(filter.value ?? {})[0]);
					} else if(filter.filter_type === 'Journal') {
						addId(maps.journals, filter.id);
					} else if(filter.filter_type === 'Item') {
						addId(maps.items, filter.id);
					} else if(filter.filter_type === 'NotFaction') {
						addFaction(maps.factions, filter.id);
					} else if(filter.filter_type === 'NotClass') {
						addId(maps.classes, filter.id);
					} else if(filter.filter_type === 'NotRace') {
						addId(maps.races, filter.id);
					} else if(filter.filter_type === 'NotCell') {
						addId(maps.cells, filter.id);
					} else if(['Dead', 'NotId'].includes(filter.filter_type)) {
						addId(maps.actors, filter.id);
					}
				});
			} else if(record.type === 'Light') {
				if(record.data?.flags && (record.data.flags & FLAG_CAN_CARRY)) {
					addId(maps.items, record.id);
				}
			} else if(record.type === 'Script') {
				if(record.text && (record.header?.num_shorts || record.header?.num_longs || record.header?.num_floats)) {
					const variables = parseScript(record);
					variables.forEach(({ name, type }) => addVariable(maps.locals, name, type));
				}
			} else if(record.type in RECORDS) {
				const type = RECORDS[record.type];
				addId(maps[type], record.id);
			}
		});
	});
	const out = { ...EMPTY_STATE };
	for(const key in maps) {
		if(key === 'types') {
			out[key] = maps[key];
			Object.keys(maps[key]).forEach(type => {
				out[key][type] = Object.entries(out[key][type]).sort(byId);
			});
		} else {
			out[key] = Object.entries(maps[key]).sort(byId).map(v => v[1]);
		}
	}
	out.factions = out.factions.filter(f => f.id !== 'FFFF');
	return out;
}

function getDialogue(records) {
	let current;
	const out = [];
	records.forEach(record => {
		if(record.flags?.[1] & FLAG_IGNORED) {
			return;
		}
		if(record.type === 'Dialogue') {
			current = {
				dial: record,
				info: []
			};
			out.push(current);
		} else if(record.type === 'Info') {
			current.info.push(record);
		}
	});
	return out;
}

export function DialogueProvider({ children }) {
	const [cache, setCache] = useState(EMPTY_STATE);
	const { files, setActive } = useDataFiles();
	const dialogue = useCached(files, getDialogue);
	useEffect(() => {
		setCache(fillCache(files));
	}, [dialogue]);
	const value = useMemo(() => {
		return { cache, setCache, files, dialogue, setActive };
	}, [cache, setCache, files, dialogue, setActive]);
	return <Context.Provider value={value} children={children} />;
}

function useDialogue() {
	return useContext(Context);
}

function insertRecord(record, records, file) {
	const index = records.findIndex(r => r.record.info_id === record.info_id);
	const override = index >= 0;
	if(override) {
		if(records[index].record.prev_id === record.prev_id) {
			records[index] = { record, file, override };
			return;
		}
		records.splice(index, 1);
	}
	for(let i = 0; i < records.length; i++) {
		const r = records[i];
		if(record.prev_id === r.record.info_id) {
			return records.splice(i + 1, 0, { record, file, override });
		}
	}
	if(record.prev_id) {
		records.push({ record, file, override });
	} else {
		records.unshift({ record, file, override });
	}
}

function loadTopic(type, topic, dialogue, files) {
	const info = [];
	dialogue.forEach((fileDialogue, i) => {
		const file = files[i];
		fileDialogue.forEach(dial => {
			if(dial.dial.dialogue_type === type && dial.dial.id.toLowerCase() === topic) {
				dial.info.forEach(record => insertRecord(record, info, file));
			}
		});
	});
	return info;
}

function getKey(type, topic) {
	if(!type || typeof topic !== 'string') {
		return null;
	}
	return `${type}_${topic}`;
}

function copySet(object, copy, key, value) {
	if(copy[key] !== value) {
		if(object === copy) {
			copy = { ...object };
		}
		copy[key] = value;
	}
	return copy;
}

function isFilterValue(value) {
	if(typeof value === 'object') {
		const keys = Object.keys(value);
		if(keys.length === 1) {
			if(keys[0] === 'Integer') {
				return Number.isInteger(value.Integer);
			} else if(keys[0] === 'Float') {
				return Number.isFinite(value.Float);
			}
		}
	}
	return false;
}

function fixNewLines(text) {
	return text.split(/\r\n|\n|\r/g).join('\r\n');
}

function fixRecord(record, prev, next, type) {
	let out = record;
	out = copySet(record, out, 'prev_id', prev?.info_id ?? '');
	out = copySet(record, out, 'next_id', next?.info_id ?? '');
	if(type === 'Journal' || !record.filters) {
		out = copySet(record, out, 'filters', []);
	} else if(record.filters) {
		let changed = false;
		const filters = record.filters.map(filter => {
			if(!FILTER_TYPES[filter.filter_type] || !FILTER_COMPARISONS[filter.filter_comparison] || !isFilterValue(filter.value)) {
				return false;
			}
			let replacement = filter;
			if(filter.filter_type === 'Function') {
				if(!FILTER_FUNCTIONS[filter.filter_function]) {
					return false;
				}
				replacement = copySet(filter, replacement, 'id', '');
			} else {
				replacement = copySet(filter, replacement, 'filter_function', FILTER_TYPE_FUNCTIONS[filter.filter_type]);
			}
			changed ||= replacement !== filter;
			return replacement;
		}).filter(Boolean);
		if(changed || filters.length !== record.filters) {
			out = copySet(record, out, 'filters', filters);
		}
	}
	if(record.data) {
		let data = copySet(record.data, out.data, 'disposition', Math.max(0, Math.floor(record.data.disposition) || 0));
		data = copySet(record.data, data, 'dialogue_type', type);
		if(!record.data.speaker_sex) {
			data = copySet(record.data, data, 'speaker_sex', 'Any');
		}
		if(!('speaker_rank' in record.data)) {
			data = copySet(record.data, data, 'speaker_rank', -1);
		}
		if(!('player_rank' in record.data)) {
			data = copySet(record.data, data, 'player_rank', -1);
		}
		out = copySet(record, out, 'data', data);
	}
	if(record.text) {
		out = copySet(record, out, 'text', fixNewLines(out.text));
	}
	if(record.result) {
		out = copySet(record, out, 'result', fixNewLines(out.result));
	}
	['speaker_id', 'speaker_rank', 'speaker_class', 'speaker_faction', 'speaker_cell', 'player_faction', 'sound_path'].forEach(key => {
		if(record[key] === '') {
			out = copySet(record, out, key, undefined);
		}
	});
	if('deleted' in record) {
		if(!out.flags) {
			out = copySet(record, out, 'flags', [0, FLAG_DELETED]);
		} else if(!(out.flags[1] & FLAG_DELETED)) {
			const flags = out.flags.slice();
			out = copySet(record, out, 'flags', flags);
			out.flags[1] |= FLAG_DELETED;
		}
	}
	return out;
}

function insertTopic(records, id, type, hint = 0) {
	const record = {
		...EMPTY_TOPIC, id, dialogue_type: type
	};
	if(type === 'Journal') {
		for(let i = hint; i < records.length; i++) {
			const r = records[i];
			if(r.type === 'Dialogue' && r.dialogue_type !== 'Journal') {
				records.splice(i, 0, record);
				return i + 1;
			}
		}
	}
	records.push(record);
	return records.length;
}

function saveTopic(setActive, type, id, records, name = id) {
	const info = [];
	records.forEach(({ file, record }, i) => {
		if(file.active) {
			info.push(fixRecord(record, records[i - 1]?.record, records[i + 1]?.record, type));
		}
	});
	setActive(prev => {
		const json = prev.json.slice();
		let index = -1;
		let spliced = false;
		let start = json.length;
		for(let i = 0; i < json.length; i++) {
			const record = json[i];
			if(index >= 0 && record.type !== 'Info') {
				json.splice(index, i - index, ...info);
				spliced = true;
				break;
			} else if(record.type === 'Dialogue') {
				start = Math.min(i, start);
				if(record.dialogue_type === type && record.id.toLowerCase() === id) {
					index = i + 1;
				}
			}
		}
		if(index < 0) {
			index = insertTopic(json, name, type, start);
			index = json.length;
		}
		if(!spliced) {
			json.splice(index, json.length - index, ...info);
		}
		return { ...prev, json };
	});
}

function setTopic(key, setCache, setActive, type, topic) {
	return value => setCache(prev => {
		const prevRecords = prev.topics[key];
		if(typeof value === 'function') {
			value = value(prevRecords);
		}
		if(prevRecords === value) {
			return prev;
		}
		if(!prevRecords && value || value.some((r, i) => r.record !== prevRecords[i]?.record || r.file.name !== prevRecords[i]?.file.name)) {
			const name = prev.types[type].find(([id]) => id === topic)?.[1];
			setTimeout(() => saveTopic(setActive, type, topic, value, name), 0);
		}
		return { ...prev, topics: { ...prev.topics, [key]: value } };
	});
}

function getDeleteRecords(setCurrent, type, topic, dialogue, files, setCache, key, setActive) {
	return records => {
		const ids = new Set();
		const override = records.reduce((out, r) => {
			ids.add(r.record.info_id);
			return out || r.override;
		}, false);
		if(override) {
			setActive(active => {
				const dial = dialogue.slice();
				dial[dial.length - 1] = getDialogue(active.json.filter(record => {
					return record.type !== 'Info' || !ids.has(record.info_id);
				}));
				setCurrent(loadTopic(type, topic, dial, files));
				return active;
			});
		} else {
			setCurrent(prev => prev.filter(({ record }) => !ids.has(record.info_id)));
		}
	};
}

function getAddTopic(setCache, setActive) {
	return (type, name) => {
		const id = name.toLowerCase();
		setCache(prev => {
			const topics = { ...prev.topics, [getKey(type, id)]: [] };
			const types = { ...prev.types };
			types[type] = types[type].concat([[id, name]]).sort(byId);
			let journals = prev.journals;
			if(type === 'Journal') {
				journals = journals.slice();
				journals.push(name);
				journals.sort((lhs, rhs) => {
					const lLhs = lhs.toLowerCase();
					const lRhs = rhs.toLowerCase();
					if(lLhs < lRhs) {
						return -1;
					} else if(lLhs > lRhs) {
						return 1;
					}
					return 0;
				});
			}
			return { ...prev, topics, types, journals };
		});
		setActive(prev => {
			const json = prev.json.slice();
			insertTopic(json, name, type);
			return { ...prev, json };
		});
	};
}

export function useTopics({ type, topic }) {
	const { cache, setCache, files, dialogue, setActive } = useDialogue();
	const key = getKey(type, topic);
	const { setCurrent, deleteRecords } = useMemo(() => {
		const set = setTopic(key, setCache, setActive, type, topic);
		return {
			setCurrent: set,
			deleteRecords: getDeleteRecords(set, type, topic, dialogue, files, setCache, key, setActive)
		};
	}, [key, setCache, setActive, dialogue]);
	useEffect(() => {
		if(type && topic) {
			if(!cache.topics[key]) {
				setCache(prev => ({ ...prev, topics: {
					...prev.topics, [key]: loadTopic(type, topic, dialogue, files)
				} }));
			}
		}
	}, [type, topic, dialogue]);
	const addTopic = useMemo(() => getAddTopic(setCache, setActive), [setCache, setActive]);
	return {
		types: cache.types,
		current: (key && cache.topics[key]) ?? null,
		setCurrent,
		deleteRecords,
		addTopic
	};
}

export function useActors() {
	return useDialogue().cache.actors;
}

export function useCells() {
	return useDialogue().cache.cells;
}

export function useClasses() {
	return useDialogue().cache.classes;
}

export function useFactions() {
	return useDialogue().cache.factions;
}

export function useGlobals() {
	return useDialogue().cache.globals;
}

export function useItems() {
	return useDialogue().cache.items;
}

export function useJournals() {
	return useDialogue().cache.journals;
}

export function useLocals() {
	return useDialogue().cache.locals;
}

export function useRaces() {
	return useDialogue().cache.races;
}
