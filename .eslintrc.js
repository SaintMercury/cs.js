/* eslint-env node */

const { dependencies } = require('./package.json');

module.exports = {
	extends: 'eslint:recommended',
	env: {
		browser: true,
		es6: true
	},
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true
		}
	},
	plugins: [
		'react'
	],
	settings: {
		react: {
			version: dependencies.react
		}
	},
	rules: {
		'array-callback-return': 'error',
		'no-duplicate-imports': 'error',
		'no-use-before-define': 'error',
		camelcase: ['error', { properties: 'never' }],
		curly: 'error',
		'dot-notation': 'error',
		eqeqeq: 'error',
		'no-array-constructor': 'error',
		'no-else-return': 'error',
		'no-empty-function': 'error',
		'no-eval': 'error',
		'no-magic-numbers': ['error', { ignore: [-1, 0, 1, 2] }],
		'no-multi-assign': 'error',
		'no-return-assign': 'error',
		'no-return-await': 'error',
		'no-shadow': 'error',
		'no-throw-literal': 'error',
		'no-useless-return': 'error',
		'no-var': 'error',
		'prefer-const': 'error',
		'brace-style': 'error',
		'comma-dangle': 'error',
		'eol-last': 'error',
		indent: ['error', 'tab'],
		'jsx-quotes': 'error',
		'linebreak-style': 'error',
		'no-multi-spaces': 'error',
		'no-multiple-empty-lines': 'error',
		'no-trailing-spaces': 'error',
		quotes: ['error', 'single'],
		semi: 'error',
		'react/no-deprecated': 'error',
		'react/no-this-in-sfc': 'error',
		'react/require-render-return': 'error',
		'react/jsx-uses-react': 'error',
		'react/jsx-uses-vars': 'error'
	}
};
